import express from "express";
import 'dotenv/config';

import surveysRouter from "./routes/surveys.routes.js";

const app = express();

const { PORT } = process.env;

// Midllewares.
app.use(express.json());

// Routes.
app.use('/surveys', surveysRouter)

// Server start.
app.listen(PORT, () => {
  console.clear();
  try {
    console.log(`Server listening on port: ${PORT}`);
  } catch (error) {
    console.log(`Server unable to start. Failed with the following message: ${error.message}`);
  }
});
