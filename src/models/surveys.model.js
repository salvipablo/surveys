import mongoose from "../config/mongo.config.js";
const ObjectId = mongoose.Types.ObjectId;

const questionSchema = new mongoose.Schema({
  questions: [
    {
      question: String,
      answers: [
        {
          answer: String,
          people_who_responded: []
        }
      ]
    }
  ]
});

const Question = mongoose.model("Questions", questionSchema);

export const allSurveys = async () => {
  try {
    const questions = await Question.find({});

    if (questions.length === 0) throw new Error('There are no questions in the database');

    return {
      message: "All questions are shipped",
      data: questions
    };
  } catch (error) {
    return {
      message: error.message,
      data: []
    };
  }
};

export const oneSurvey = async (id) => {
  try {
    const questions = await Question.find({ "_id": new ObjectId(id) });

    if (questions.length === 0) throw new Error('Survey search not found in the database');

    return {
      message: "One Survey",
      data: questions
    };
  } catch (error) {
    return {
      message: error.message,
      data: []
    };
  }
};

export const report = async (id) => {
  try {
    const data = await Question.aggregate([
      {
        $match: {
          "_id": new ObjectId(id)
        }
      },
      {
        $unwind: "$questions"
      },
      {
        $unwind: "$questions.answers"
      },
      {
        $group: {
          _id: "$questions.answers.answer",
          total_people: { $sum: { $size: "$questions.answers.people_who_responded" } }
        }
      }
    ]);

    if (data.length === 0) throw new Error('El reporte no encontro resultados');

    return {
      message: "reports",
      data: data
    };
  } catch (error) {
    return {
      message: error.message,
      data: []
    };
  }
}

export const create = async (dataForSurvey) => {
  try {
    const question = await Question.create(dataForSurvey);

    return {
      message: "Survey created",
      data: question
    };
  } catch (error) {
    return {
      message: error.message,
      data: {}
    };
  }
};

export const update = async (idSurvey, dataForSurvey) => {
  try {
    // TODO: Este actualiza el registro pero no agrega la persona nueva que voto.
    const updateSurvey = await Question.findByIdAndUpdate({ _id: idSurvey }, dataForSurvey);

    if (!updateSurvey) {
      throw new Error('The Product you are trying to update is not found in the database');
    }

    return {
      message: "Survey successfully updated",
      data: updateSurvey
    };
  } catch (error) {
    return {
      message: error.message,
      data: {}
    };
  }
}; 
