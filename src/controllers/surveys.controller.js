import {
  allSurveys,
  oneSurvey,
  report,
  create,
  update
} from "../models/surveys.model.js";

export const getAllSurveys = async (req, res) => {
  const operationAllSurveys = await allSurveys();

  const statusCode = operationAllSurveys.data.length === 0 ? 404 : 200;

  return res.status(statusCode).json({
    message: operationAllSurveys.message,
    data: operationAllSurveys.data
  });
};

export const getOneSurvey = async (req, res) => {
  const operationOneSurvey = await oneSurvey(req.params.id);

  const statusCode = operationOneSurvey.data.length === 0 ? 404 : 200;

  return res.status(statusCode).json({
    message: operationOneSurvey.message,
    data: operationOneSurvey.data
  });
};

export const getReport = async (req, res) => {
  const operationReport = await report(req.params.id);

  const statusCode = operationReport.data.length === 0 ? 404 : 200;

  return res.status(statusCode).json({
    message: operationReport.message,
    data: operationReport.data
  });
};

export const createSurvey = async (req, res) => {
  const operationCreate = await create(req.body);

  //const statusCode = operationReport.data.length === 0 ? 404 : 200;
  let statusCode = 200;

  return res.status(statusCode).json({
    message: operationCreate.message,
    data: operationCreate.data
  });

};

export const updateSurvey = async (req, res) => {
  const operationOneSurvey = await update(req.params.id, req.body);

  const statusCode = !operationOneSurvey.data ? 404 : 200;

  return res.status(statusCode).json({
    message: operationOneSurvey.message,
    data: operationOneSurvey.data
  });
};
