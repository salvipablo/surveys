import express from "express";

import {
  getAllSurveys,
  getOneSurvey,
  getReport,
  createSurvey,
  updateSurvey,
} from "../controllers/surveys.controller.js";


const surveysRouter = express.Router();

surveysRouter.get('/', getAllSurveys);
surveysRouter.get('/:id', getOneSurvey);
surveysRouter.get('/report/:id', getReport);
surveysRouter.post('/', createSurvey);
surveysRouter.put('/:id', updateSurvey);

export default surveysRouter;